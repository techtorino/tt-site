<?php
function mychildtheme_enqueue_styles() {
    $parent_style = 'lana-site';
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'tt-site',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
}
add_action( 'wp_enqueue_scripts', 'ttsite_enqueue_styles' );
?>